# TP1


El Tp 1 se conformo con Multiples Put Servers y Get Servers, donde la logica de negocio podria estar en estos servidores y en caso de ser necesario, escalarlos horizontalmente de manera independiente. 

El storage server se implementa como un solo contenedor, donde tiene la persistencia de archivos.

Toda comunicacion de servicios ocurre a traves de sockets TCP, siguiendo en general un formato de START-MSG-END, donde START y END son chars ASCII definidos y MSG depende del tipo de mensaje.


## Diagrama de Actividades
![alt text](DiagramaSecuenciaTp1.png "Diagrama de Actividades")



## Diagrama de Robustez
![alt text](RobustezEj1-2.png "Diagrama de Robustez")


## Build

Todo el codigo esta dentro de la carpeta tp1-web

Para correr el TP es necesario Docker-compose, preferentemente las versiones mas recientes

Para buildear el repositorio correrlo con 
```bash
docker-compose up --build
```

### Tests

Para correr el test solo hace falta correr el test suite de cargo

```bash
cargo test
```

### Correr ejemplos

Una vez el server levantado, se pueden correr los distintos binarios que se comunicarian contra el servidor de docker

```bash
cargo run --bin client1
cargo run --bin client2
cargo run --bin client3
cargo run --bin spawnner
```

## Deuda Tecnica

* Logger a pantalla activable por variables de ambiente
* Implementacion de lectura Multithread dentro del Storage Server para poder leer y filtrar en paralelo 
  
🔴 Se utiliza una dirección IP fija en 0.0.0.0 para la establecer la conexión de los servidores (src/server.rs::72). Estos datos no deberían hardcodearse en el código sino que sería mejor tenerlos en variables constantes, aunque el mejor caso de sería el de pasarlos como parámetro. Por otro lado, el sistema funciona con esa dirección ya que se mappean los puertos del Docker Compose para que luego pueda conectarse por el localhost, debería utilizarse el servicio de DNS provisto para que el sistema se comunique directamente a través de la red de Docker y no del host.

En este caso se decide levantar el servidor en la ip 0.0.0.0, para poder escuchar a todos los clientes accesibles desde el contenedor. Esto es si uso la resolucion de nombres del contenedor como get_server o put_server. A su vez, se le pasa como hostname de los servidores por variable de ambiente a los clientes. Para el unico caso donde se conectan por localhost es para clientes levantados de manera local para hacer pruebas, pero si el puerto no se expone al host funciona de igual manera

```yml

  client1:
    build:
      context: .
      dockerfile: Dockerfile.runner
    command: "bash -c 'sleep 5s; client1'"
    environment:
      - PUT_SERVER_HOST=put_server
      - GET_SERVER_HOST=get_server
  
  client2:
    build:
      context: .
      dockerfile: Dockerfile.runner
    command: "bash -c 'sleep 5s; client2'"
    environment:
      - PUT_SERVER_HOST=put_server
      - GET_SERVER_HOST=get_server2
      - SERVER_ID=2

```

Algo sin embargo que puede prestar a la confusion es que en la creacion de los clientes se le pasa "localhost" como primer parametro, sin embargo no es usado en la implementacion. Esto lo voy a corregir


🔴 No se chequea el resultado del FileManager::append para saber si la escritura fue exitosa (src/handlers/database/database_write_handler.rs::115).

Abajo el res de hecho se matchea contra un Result, si da OK, caso de que la escritura fuera exitosa, se escribe OK_WRITE, si da ERR se escribe sobre el socket ERR_WRITE. 


```rust
    let res = FileManager::append(&entry, message);
    match res {
        Ok(_val) => {
            let msg = format!("{}{}{}", COM_START, OK_WRITE, COM_END);
            stream.write_all(msg.as_bytes()).unwrap();
        }
        Err(_) => {
            let msg = format!("{}{}{}", COM_START, ERR_WRITE, COM_END);
            stream.write_all(msg.as_bytes()).unwrap();
        }
```

La respuesta de este socket la recibe el storage manager (src/storage_manager::43), donde se matchea con el stream OK_WRITE; caso de que sea escritura exitosa, o valor distinto a OK_WRITE, caso de error, lo que le devuelve el Err al que llame la funcion put de put_logger_handler.

```rust
        let response = storage_client.message(&message, ServerType::WRITE);
        StorageManager::flog(format! {"Response {:?}",response});

        match response.as_str() {
            OK_WRITE => Ok("Ok"),
            _ => Err("Err"),
        }
```



🔴 Dado que múltiples handlers pueden escribir en un mismo archivo, podría ocurrir que alguno quede desordenado y por ende se le termine devolviendo al usuario una lista de logs desordenada. Hay varias opciones para corregir esto, una es reordenar en memoria los archivos de logs al obtenerlos, otras podría ser que cada writer solo acceda a un subconjunto de archivos de logs, de manera que cada archivo quede ordenado (para esto se podría usar una función de hashing) y luego los readers hagan un merge ordenado del mismo.


El storage manager, corriendo desde los clientes, es el que se encarga de sortear los mensajes una vez recibidos, al splitear los logs por \n y eliminar las lineas vacias. (src/storage_manager.rs::86).


```rust

 match response.as_str() {
    INVALID_ID => return Err(INVALID_ID),
    val => {
        let mut vec_splits = val
            .split('\n')
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>();
        StorageManager::flog(format! {"split {:?}",vec_splits});
        vec_splits.sort();
        StorageManager::flog(format! {"sorted {:?}",vec_splits});
        let mut vec_joint = vec_splits.join("\n").to_string();
        StorageManager::flog(format! {"joint {:?}",vec_joint});
        vec_joint.push_str("\n");
        return Ok(vec_joint);
    }
};

```

# Reentrega


### Completos
🔴 [OK] Tampoco se chequea que el método create_by_id_date efectivamente haya podido crear el archivo, ignorándose el dato de retorno (src/managers/file_manager.rs::144).

> Se implementa validacion al momento de creacion

🔴 [OK] Al escribir un mensaje a través del cliente con el write, no se está chequeando que la cantidad de bytes enviados sea la esperada (src/client.rs::74), ya que podría ocurrir que se terminen enviando menos. Esta es una parte clave del sistema, ya que en caso de que se envíen menos bytes de los esperados debería procederse a completar el envío (o bien, utilizar la función write_all que por debajo se encarga de ello).

> Se implementa la funcion write_all

🔴 [OK] No deberían spawnearse threads por cada conexión entrante (src/server.rs::109), ya que en caso de recibir una gran cantidad de conexiones al mismo tiempo podría generar que se lancen muchísimos threads en paralelo y ahogar al sistema. Debería en su lugar utilizarse un pool de threads de manera de limitar la cantidad máxima en paralelo.

> Implementado segun threadpool https://www.fatalerrors.org/a/0Npz0Dw.html

🔴 [OK] El método read_file del FileManager lee todo el archivo y lo levanta en memoria (src/managers/file_manager.rs::19-29). Esto claramente genera un problema ya que para archivos muy grandes vamos a consumir demasiados recursos, pudiendo tirar abajo el sistema. Los logs deberían ir leyéndose línea por línea e ir descartándose los que no apliquen según los filtros.

>Se implementa el read_to_string de BufRead, donde se itera por cada linea y se escribe en el string. En este caso es para los archivos donde no se especifica algun filtro sea por fecha o por tag.


🔴 [OK] No se utilizan locks para agregar nuevos logs en los archivos del Storage en el método append (src/managers/file_manager.rs::148). Esto genera que si se envían varios logs con un mismo AppId y con una misma fecha, las escrituras del archivo en paralelo puedan solaparse generando que se corrompan. Además, el método write_all no es atómico, lo que también genera la necesidad de utilizar locks.

>Se crea una nueva estructura dentro del file manager donde se almacenan los locks de los archivos usados, creando si no existen

🔴 [OK] No estaba hecho el diagrama de secuencia/actividades al momento de la entrega. Recordar que diagramas/informe son una parte de la entrega de TPs tan importante como el código mismo.

>Se completo para antes de las 12 del dia de la entrega, tarde para la exposicion de igual modo.

🔴 [OK] Hay mucho código duplicado entre los cuatro Handlers (GetLoggerHandler, PutLoggerHandler, DatabaseReadHandler y DatabaseWriteHandler) que podría haberse sacado a un archivo/librería común. Hay varios otros casos en el resto del código, por ejemplo la función validate_start que se mencionó anteriormente.

>Se refactorizo validate_start y get_splits a un archivo utils, y handle_error a la implementacion del trait Handler



🟡 [OK] No se utiliza el método close del ServerMaster (src/storage/master.rs::57). De todas formas, los servidores dedicados a la escritura y lectura se cierran directamente en el set_handler del SIGINT/SIGTERM, por lo que simplemente podría borrarse dicho close.

>Borrado close


🟡 [OK] Así mismo, hay mucho código no funcional que quedó mezclado con el código del sistema en sí. Por ejemplo, el PING/PONG que hacen los handlers (src/handlers/logger/put_logger_handler.rs::110-114), o el mensaje custom del server (src/server.rs::133). Mucho de este código se utiliza sólo para testear, por lo que no debería ser parte del sistema.

>Se elimino la contemplacion de mensaje PING


🟡 [OK] El hecho de tomar un start_date y un end_date por defecto en caso de que no se puedan obtener los verdaderos en el FileManager (src/managers/file_manager.rs::107-113) podría generar que algunos logs se guarden en archivos incorrectos por un error en la fecha, de manera que no sería fácil encontrarlos después. En ese caso sería mejor fallar y no proceder a registrar el log.

>En realidad se toman por defecto si no se pasa ninguno por parametro, es decir, que si no hay algun filtro disponible, se toma el valor de referencia como minimo de fecha definido por el sistema y maximo de fecha, para poder hacer una comparacion. Como minimo se toma el año 0000 y maximo el año 9999

Documentación:
🔴 [OK] Diagrama de Robustez: El diagrama no sigue la notación vista en la cátedra. La misma es vital porque permite entender de qué tipo de componentes estamos hablando, ya que pueden ser Controllers, Entities o Interfaces y en este diagrama no queda claro. Falta también el actor que haga de punto de entrada al sistema. Y el tamaño en general del diagrama es muy chico, hace falta hacer zoom en el informe para poder leerlo, cuando debería poder entenderse todo lo que dice a simple vista.
## Diagrama de Robustez
![alt text](RobustezEj1-2.png "Diagrama de Robustez")

🔴 [OK] Hay mucho código comentado que debería haberse limpiado antes de la entrega final.


🟡 [OK] Hay un error conceptual en la utilización del Semaphore del Server (src/server.rs::96). Se usa una implementación distinta a la estándard, que precisa de una variable resource (el handle) que después no se vuelve a utilizar en todo el flujo. Aparte, se está utilizando el semáforo simplemente como un contador para ver cuántas conexiones en paralelo hay.

>Se agrego un contador donde se decrementa o incrementa de acuerdo a conexiones, y si son mayores que el limite no se acepta la conexion



🟡 [REVISAR] Hay un error conceptual en cómo se realiza el shutdown del Server, ya que se está generando una conexión hacia el listener dentro del mismo servidor sólo para desbloquear el incoming (src/server.rs::99) y así poder llegar al break. Debería buscarse la forma de poder cerrar directamente el TcpListener sin tener que generar esta conexión extra, por ejemplo con estas posibles soluciones.

>Vi el ejemplo propuesto, donde se implementa usando un listener no bloqueante. Esto tiene un impacto de que en caso de hacer un incoming y no haber ningun TcpStream entonces hay que manejar que hacer con ese thread, si es hacer un sleep o volver a chequear si hay una conexion entrante. Esto puede tener una consecuencia de que consuma mucho CPU si no se hace un sleep, o que se pongan demoras mientras no se esta aceptando alguna conexion. Tambien existen alternativas como cerrar el socket usando libc, sin embargo hay que usar unsafe para poder cerrar el socket.

https://stackoverflow.com/a/59474177

🟡 [OK] A lo largo del código se encuentran muchos valores hardcodeados (por ejemplo para valores por defecto de puertos o parámetros del sistema como el directorio root de storage) que sería mejor que no estuviesen sueltos sino que se utilizasen desde una variable (como un const en algún archivo de properties, como el utils.rs).

>Se refactorizaron las siguientes variables.

```rust
pub const STORAGE_SERVER_GET_PORT: usize = 1436;
pub const STORAGE_SERVER_PUT_PORT: usize = 1437;
pub const LOG_SERVER_GET_PORT: usize = 1336;
pub const LOG_SERVER_PUT_PORT: usize = 1337;
pub const DEFAULT_AMOUNT_CONNECTIONS: usize = 5;
pub const DEFAULT_AMOUNT_THREADS: usize = 8;
pub const DEFAULT_STORAGE_ROOT: &str = "storage";
```

### Restantes



🟡 Diagrama de Actividades: Se mezcla un poco la lógica de los componentes utilizados. Por momentos los mismos son una funcionalidad (i.e. server::handle_connection) y por otros momentos son un tipo de mensaje (i.e. SERVER UNAVAILABLE).
Código:


🟡 Mientras que si hay una optimización para el filtro de logs por fechas (almacenando los logs en distintos archivos según el día), no lo hay cuando se quiere filtrar por tags.


Demo: 
🟢 Gran demo, analizando distintos casos de uso con test unitarios e integradores, con clientes funcionales preparados para varios escenarios distintos.
🟢 Gran nivel de abstracción utilizado en el Client que permite que el mismo se utilice tanto para realizar la conexión entre los clientes y los servidores de GET/PUT, así como entre dichos servers y el de Storage. Ídem para el Server.
🟢 Bien abstraída la lógica del validate_start a una función aparte que permita clarificar su objetivo y simplifique el análisis del código.
🟢 Buen nivel de testing, cubriendo distintos escenarios posibles.
Diseño:
🟢 Resuelve correctamente la problemática planteada, con una correcta modularización.