use std::fs::File;
use std::fs::OpenOptions;
use std::io::Read;
use std::io::Write;
use std::time;
// #[test]
#[allow(dead_code)]
fn test_write_read_file() {
    let output = "Hello, world!";
    let filename = "hello_world.txt";
    File::create(filename)
        .unwrap()
        .write("RICH".as_bytes())
        .unwrap();
    std::thread::spawn(move || {
        let mut ofile = OpenOptions::new()
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();

        let seconds = time::Duration::from_secs(3);
        std::thread::sleep(seconds);
        // let mut ofile = File::open("hello_world.txt").expect("unable to create file");
        ofile.write_all(output.as_bytes()).expect("unable to write");
    });
    let t = std::thread::spawn(move || {
        let mut input = String::new();
        for _ in 1..10 {
            let mut single_input = String::new();
            let time = time::Duration::from_millis(400);
            std::thread::sleep(time);

            let mut ifile = OpenOptions::new().read(true).open(filename).unwrap();
            // let mut ifile = File::open("hello_world.txt").expect("unable to open file");
            ifile
                .read_to_string(&mut single_input)
                .expect("unable to read");
            println!("{}", single_input);
            input = input + &single_input;
        }
        input
    });
    let input = t.join().unwrap();
    // compare input with output
    assert_eq!(
        "RICHRICHRICHRICHRICHRICHRICHRICHHello, world!RICHHello, world!", input,
        "not equal"
    );
}
