use chrono::{TimeZone, Utc};
use serial_test::serial;
use std::fs;
use std::sync::Arc;

use std::thread::spawn;
use tp1logger::client::Client;
use tp1logger::handlers::database::DatabaseReadHandler;
use tp1logger::handlers::database::DatabaseWriteHandler;
use tp1logger::handlers::logger::{GetLoggerHandler, PutLoggerHandler};
use tp1logger::managers::FileManager;
use tp1logger::server::{Server, ServerType};

#[test]
fn test_add() {
    assert_eq!(tp1logger::add(1, 2), 3);
}

// #[test]
// fn test_send() {
//     let server = Server::new(ServerType::PUT);
//     let client = Client::new( (1336, 1337));
//     client.put_log("144D".to_string(), "Hello💖!".to_string());
//     assert_eq!(server.get_message(), "PUT 144D\nHello💖!");
// }

#[test]
#[serial]
fn test_client_ping_incorrect_format() {
    let server = Server::new(ServerType::PUT);
    let client = Client::new( (1336, 1337));
    println!("Server Running");
    let server_arc = Arc::new(server);
    let server_1 = server_arc.clone();
    let t_server = spawn(move || {
        let handler = PutLoggerHandler {};
        server_1.run(handler);
    });

    let response = client.ping();
    assert_eq!(response, "Formato de log incorrecto");

    let response = client.ping();
    assert_eq!(response, "Formato de log incorrecto");

    server_arc.close();
    t_server.join().unwrap();
}

#[test]
#[serial]
fn test_client_big_message() {
    let server = Server::new(ServerType::PUT);
    let client = Client::new( (1336, 1337));
    println!("Server Running");
    let server_arc = Arc::new(server);
    let server_1 = server_arc.clone();
    let t_server = spawn(move || {
        let handler = PutLoggerHandler {};
        server_1.run(handler);
    });

    let string="✨fVqvJkZtIo3ZTcZByjIMm2oF7kzuBQTEqk9bcf8JpAGHRn1E96eYw03h67rzzjj0R2lzjP6xztFH1eUHkAhXygJiR78vhQ5lYa9F5JDV9sFmQ5RVeXlSgAIVv7OqjSnYY731A1HA93Hj7a6CrW8tlCjD2OL4jiJRW6yZKhyW3ppveerFVx7JvEwvOTGkSPM6jEE8PAIf4kSWmgvJ4os81dJcIGWWQFQpI0x4lGBdfSc2oIDurwcL1e97fzlnW2F12fDOOsdbwzotTDoS6fsTpCFGueMDOoSJPDzNMU4MTH9pfKUJchY052RiTrNICitsfsPBY2BBg59KZrLvG3gy4qfyqceZdUP9fYGGHVJtU66PIz0yAS36zALWh9lRtBH0I3mxGFcIWaqljr5A7JBEyfqdHkfcuB1rZ5gcgVmwlcKCvbDBMNJFRaL2U2q1HbdPaPIKQsL8gv30e6hbTxnfBlio98ykoYx5mJ9Y6hpCc81LJttMXTDPzao8PzGajRzcJiY5Lqpi39MqISVEdq45xSpOagb175Bbp2L0tgsJ8dUjBh4C9I5KhHPjuXq25iDNQZ0P4hhvuJ6a8hdsr3T8RqUP7gHFeZlnehNThGHzo0uCcAQFMLss1mqL2A7ZrCjlpNWCdtvYrHeEUv13BaauYwtVhAcQs9Uih03QUzbRLjHAI4eEZobpspRHnLUdfqEPzGGtR4cGQ646TeR3UR66OGffXRXarwz6K5p2MIGDOoWtEmYR0uDjOD6NtmWIm3u05N1YuXarcuaLi5fzdKkMaLF97pKVMBHtdCevMFVqodDHpccF4WMZyhoXKPrLVNyaYiCLz9lG0V7IPHHc1n8rzD8dn9eLhhd12FQU9RqeXFaj0gmiNbXqgY7uRcQ9lnIjxOJY7Hm3dLbFP8WD7aZ8Bp0RCM4nCXJ6vqKpc2V87c7onBDAH3g6oxIkxXazWv4bnwMLJAJQf98MsHwx0ifnT6MJ5CJWLPN3O02dSOCUdWyVTzTILSkMLPKxyXvO\x04";
    let response = client.message(string, ServerType::PUT);
    assert_eq!(response, "Formato de log incorrecto");

    server_arc.close();
    t_server.join().unwrap();
}

#[test]
#[serial]
fn test_client_put_log() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
    let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });

    let server = Server::new(ServerType::PUT);
    let client = Client::new( (1336, 1337));
    println!("Server Running");
    let server_arc = Arc::new(server);
    let server_1 = server_arc.clone();
    let t_server = spawn(move || {
        let handler = PutLoggerHandler {};
        server_1.run(handler);
    });

    let string = "Hola";
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric", "123", "Debug"]);
    assert_eq!(response, "Log creado correctamente");

    server_arc.close();
    t_server.join().unwrap();

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

fn clean_storage() {
    let _remove = fs::remove_dir_all("storage");
    fs::create_dir("storage").unwrap();
}

#[test]
#[serial]
fn test_client_put_log_invalid_format() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
    let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });

    let server = Server::new(ServerType::PUT);
    let client = Client::new( (1336, 1337));
    println!("Server Running");
    let server_arc = Arc::new(server);
    let server_1 = server_arc.clone();
    let t_server = spawn(move || {
        let handler = PutLoggerHandler {};
        server_1.run(handler);
    });

    let string = "Hola";
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric!", "123", "Debug"]);
    assert_eq!(response, "Formato de log incorrecto");

    server_arc.close();
    t_server.join().unwrap();

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_client_get_log_inexistant() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
    let file_manager = FileManager::new();

    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });

    let server = Server::new(ServerType::GET);
    let client = Client::new( (1336, 1337));
    println!("Server Running");
    let server_arc = Arc::new(server);
    let server_1 = server_arc.clone();
    let t_server = spawn(move || {
        let handler = GetLoggerHandler {};
        server_1.run(handler);
    });

    let string = "Hola";
    let response = client.get_log("appId123", Some(string), None, None, None);
    assert_eq!(&response, "AppId recibido no existe");

    server_arc.close();
    t_server.join().unwrap();
    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_client_get_log_sorted() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
    let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });

    let client = Client::new( (1336, 1337));

    let server = Server::new(ServerType::PUT);
    let server_arc_put = Arc::new(server);
    let server_1 = server_arc_put.clone();
    let t_server_put = spawn(move || {
        let handler = PutLoggerHandler {};
        server_1.run(handler);
    });

    let server = Server::new(ServerType::GET);
    let server_arc_get = Arc::new(server);
    let server_2 = server_arc_get.clone();
    let t_server_get = spawn(move || {
        let handler = GetLoggerHandler {};
        server_2.run(handler);
    });

    let string = "Hola";
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric", "123", "Debug"]);
    assert_eq!(response, "Log creado correctamente");
    let string = "Hola";
    let dt = Utc.ymd(2012, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric", "123", "Debug"]);
    assert_eq!(response, "Log creado correctamente");
    let dt = Utc.ymd(2015, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric", "123", "Debug"]);
    assert_eq!(response, "Log creado correctamente");
    let dt = Utc.ymd(2015, 7, 8).and_hms(1, 10, 11); // `2014-07-08T09:10:11Z`
    let response = client.put_log("appId123", dt, string, vec!["Ric", "123", "Debug"]);
    assert_eq!(response, "Log creado correctamente");

    let response = client.get_log("appId123", None, None, None, None);
    assert_eq!(&response, "[appId123] 2012-07-08 09:10:11 UTC [Ric,123,Debug] Hola\n[appId123] 2014-07-08 09:10:11 UTC [Ric,123,Debug] Hola\n[appId123] 2015-07-08 01:10:11 UTC [Ric,123,Debug] Hola\n[appId123] 2015-07-08 09:10:11 UTC [Ric,123,Debug] Hola\n");

    server_arc_get.close();
    t_server_get.join().unwrap();
    server_arc_put.close();
    t_server_put.join().unwrap();

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}
