use chrono::{TimeZone, Utc};
use serial_test::serial;
use std::fs;
use std::sync::Arc;
use std::thread::spawn;
use tp1logger::handlers::{
    database::{DatabaseReadHandler, DatabaseWriteHandler},
    logger::{ReadJob, WriteJob},
};
// use tp1logger::handllers::;
use tp1logger::server::{Server, ServerType};
use tp1logger::storage_manager::StorageManager;
use tp1logger::managers::FileManager;

fn clean_storage() {
    let _remove = fs::remove_dir_all("storage");
    fs::create_dir("storage").unwrap();
}

#[test]
#[serial]
fn test_storage_server_write_only() {
    clean_storage();
    let storage_server = Server::new(ServerType::CUSTOM(1437));
    let server_arc = Arc::new(storage_server);
    let server_1 = server_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let job = WriteJob {
        app_id: "123".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: Utc::now(),
    };
    let _dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");

    server_arc.close();
    t_server1.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_write_read() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let _dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: _dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: _dt,
    };
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: None,
        pattern: None,
    };

    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(
        response,
        "[123] 2014-07-08 09:10:11 UTC [Info] Ric\n[123] 2014-07-08 09:10:11 UTC [Info] Richard\n"
    );

    // let string = "Hola";
    // let response = client.put_log("appId123", string, vec!["Ric", "123", "Debug"]);

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_read_similar_ids() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let _dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let _dt2 = Utc.ymd(2016, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123a".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: _dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: _dt,
    };
    let job3 = WriteJob {
        app_id: "ID-123".to_string(),
        message: "New Message".to_string(),
        tags: vec!["Info".to_string(), "Debug".to_string()],
        date: _dt2,
    };
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job3).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: None,
        pattern: None,
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(response, "[123] 2014-07-08 09:10:11 UTC [Info] Richard\n");

    // let string = "Hola";
    // let response = client.put_log("appId123", string, vec!["Ric", "123", "Debug"]);

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_read_from() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let dt2 = Utc.ymd(2016, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123a".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job3 = WriteJob {
        app_id: "123".to_string(),
        message: "New Message".to_string(),
        tags: vec!["Info".to_string(), "Debug".to_string()],
        date: dt2,
    };
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job3).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: Some(Utc.ymd(2015, 9, 8).and_hms(9, 10, 11)),
        to: None,
        tag: None,
        pattern: None,
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(
        response,
        "[123] 2016-09-08 09:10:11 UTC [Info,Debug] New Message\n"
    );

    // let string = "Hola";
    // let response = client.put_log("appId123", string, vec!["Ric", "123", "Debug"]);

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_read_tag() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let dt2 = Utc.ymd(2016, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job3 = WriteJob {
        app_id: "123".to_string(),
        message: "New Message".to_string(),
        tags: vec!["Info".to_string(), "Debug".to_string()],
        date: dt2,
    };
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job3).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: Some("Info".to_string()),
        pattern: None,
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(
        response,
        "[123] 2014-07-08 09:10:11 UTC [Info] Ric\n[123] 2014-07-08 09:10:11 UTC [Info] Richard\n[123] 2016-09-08 09:10:11 UTC [Info,Debug] New Message\n"
    );

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: Some("Debug".to_string()),
        pattern: None,
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(
        response,
        "[123] 2016-09-08 09:10:11 UTC [Info,Debug] New Message\n"
    );

    // let string = "Hola";
    // let response = client.put_log("appId123", string, vec!["Ric", "123", "Debug"]);

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_read_to_range() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
    let _file_manager = FileManager::new();
    
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let dt2 = Utc.ymd(2016, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let dt3 = Utc.ymd(2019, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: dt2,
    };
    let job3 = WriteJob {
        app_id: "123".to_string(),
        message: "New Message".to_string(),
        tags: vec!["Info".to_string(), "Debug".to_string()],
        date: dt3,
    };

    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job3).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: Some(Utc.ymd(2014, 8, 8).and_hms(9, 10, 11)),
        to: Some(Utc.ymd(2016, 11, 8).and_hms(9, 10, 11)),
        tag: None,
        pattern: None,
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(response, "[123] 2016-09-08 09:10:11 UTC [Info] Richard\n");

    // let string = "Hola";
    // let response = client.put_log("appId123", string, vec!["Ric", "123", "Debug"]);

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}

#[test]
#[serial]
fn test_storage_server_read_pattern() {
    clean_storage();
    let storage_server_write = Server::new(ServerType::CUSTOM(1437));
    let storage_server_read = Server::new(ServerType::CUSTOM(1436));
    let server_write_arc = Arc::new(storage_server_write);
    let server_read_arc = Arc::new(storage_server_read);
    let server_1 = server_write_arc.clone();
    let server_2 = server_read_arc.clone();
let file_manager = FileManager::new();
    let t_server1 = spawn(move || {
        let handler = DatabaseWriteHandler { file_manager };
        server_1.run(handler);
    });
    let t_server2 = spawn(move || {
        let handler = DatabaseReadHandler {};
        server_2.run(handler);
    });
    let dt = Utc.ymd(2014, 7, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let dt2 = Utc.ymd(2016, 9, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
    let job = WriteJob {
        app_id: "123".to_string(),
        message: "Ric".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job2 = WriteJob {
        app_id: "123".to_string(),
        message: "Richard".to_string(),
        tags: vec!["Info".to_string()],
        date: dt,
    };
    let job3 = WriteJob {
        app_id: "123".to_string(),
        message: "New Message".to_string(),
        tags: vec!["Info".to_string(), "Debug".to_string()],
        date: dt2,
    };
    let storage_manager = StorageManager::default();
    let response = storage_manager.put(job).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job2).unwrap();
    assert_eq!(response, "Ok");
    let response = storage_manager.put(job3).unwrap();
    assert_eq!(response, "Ok");

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: None,
        pattern: Some("Ric".to_string()),
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(
        response,
        "[123] 2014-07-08 09:10:11 UTC [Info] Ric\n[123] 2014-07-08 09:10:11 UTC [Info] Richard\n"
    );

    let read_job = ReadJob {
        app_id: "123".to_string(),
        from: None,
        to: None,
        tag: None,
        pattern: Some("char".to_string()),
    };
    let response = storage_manager.get(read_job).unwrap();
    assert_eq!(response, "[123] 2014-07-08 09:10:11 UTC [Info] Richard\n");

    server_write_arc.close();
    t_server1.join().unwrap();
    server_read_arc.close();
    t_server2.join().unwrap();
}
