use chrono::TimeZone;
use chrono::Utc;


use tp1logger::client::Client;





fn main() {
    let client = Client::new((1336, 1337));
    let message = "Richardson";
    for i in 1..10 {
        let dt = Utc.ymd(2014, i, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
        let res = client.put_log("d123", dt, message, vec!["Ric", "123", "Debug"]);
        println!("{}", res);
    }
    let res = client.get_log("d123", None, None, None, None);
    println!("{}", res);
}
