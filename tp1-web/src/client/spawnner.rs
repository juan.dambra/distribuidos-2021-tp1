use chrono::TimeZone;
use chrono::Utc;

use tp1logger::client::Client;
use tp1logger::utils::{OK_PUT, UNAVAILABLE_SERVER};

fn main() {
    for i in 1..100 {
        let _t = std::thread::spawn(move || {
            let client = Client::new( (1336, 1337));
            let message = "Richardson";
            let dt = Utc.ymd(2000 + i, 1, 1).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
            loop {
                let res = client.put_log("spawnner", dt, message, vec!["Ric", "123", "Debug"]);
                println!("{}", res);
                match res.as_str() {
                    OK_PUT => break,
                    _ => println!("Repeating Put"),
                };
            }
        });
    }

    for _i in 1..100 {
        let _t = std::thread::spawn(move || {
            let client = Client::new( (1336, 1337));
            let _response = String::new();
            let from = Utc.ymd(2000 + 0, 1, 1).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
            let to = Utc.ymd(2000 + 100, 1, 1).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
            let res = client.get_log("spawnner", None, None, Some(from), Some(to));
            println!("{}", res);
        });
    }

    let _t = std::thread::spawn(move || {
        let client = Client::new( (1336, 1337));
        let mut response = String::new();
        let from = Utc.ymd(2000 + 0, 1, 1).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
        let to = Utc.ymd(2000 + 100, 1, 1).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
        loop {
            let res = client.get_log("spawnner", None, None, Some(from), Some(to));
            println!("{}", res);
            match res.as_str() {
                UNAVAILABLE_SERVER => println!("Repeating GET"),
                all => {
                    response.push_str(all);
                    break;
                }
            };
        }
        println!("Response {}", response);
    });
}
