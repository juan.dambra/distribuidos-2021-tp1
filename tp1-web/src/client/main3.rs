use chrono::TimeZone;
use chrono::Utc;

use tp1logger::client::Client;
use tp1logger::utils::{OK_PUT, UNAVAILABLE_SERVER};

fn main() {
    for i in 1..10 {
        let _t = std::thread::spawn(move || {
            let client = Client::new((1336, 1337));
            let message = "Richardson";
            let dt = Utc.ymd(2014, i, 8).and_hms(9, 10, 11); // `2014-07-08T09:10:11Z`
            loop {
                let res = client.put_log("d123", dt, message, vec!["Ric", "123", "Debug"]);
                println!("{}", res);
                match res.as_str() {
                    OK_PUT => break,
                    _ => println!("Repeating Put"),
                };
            }
        });
    }
    let client = Client::new( (1336, 1337));
    let mut response = String::new();
    loop {
        let res = client.get_log("d123", None, None, None, None);
        println!("{}", res);
        match res.as_str() {
            UNAVAILABLE_SERVER => (),
            all => {
                response.push_str(all);
                break;
            }
        };
    }
    println!("Response {}", response);
}
