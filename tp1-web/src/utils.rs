pub const VEC_SEP: &str = "\x07";
pub const COM_END: &str = "\x04";
pub const COM_DELIMITER: &str = "\x03";
pub const COM_START: &str = "\x02";
pub const UNAVAILABLE_SERVER: &str = "Servidor no disponible";
pub const INCORRECT_FORMAT: &str = "Formato de log incorrecto";
pub const OK_PUT: &str = "Log creado correctamente";
pub const ERR_PUT: &str = "Problema al escribir";
pub const ERR_GET: &str = "Problema al Obtener";
pub const OK_WRITE: &str = "Write OK";
pub const ERR_WRITE: &str = "Write ERR";
pub const OK_READ: &str = "READ OK";
pub const INVALID_ID: &str = "Id Invalido";
pub const CLIENT_INVALID_ID: &str = "AppId recibido no existe";

pub const STORAGE_SERVER_GET_PORT: usize = 1436;
pub const STORAGE_SERVER_PUT_PORT: usize = 1437;
pub const LOG_SERVER_GET_PORT: usize = 1336;
pub const LOG_SERVER_PUT_PORT: usize = 1337;

pub const DEFAULT_AMOUNT_CONNECTIONS: usize = 5;
pub const DEFAULT_AMOUNT_THREADS: usize = 8;

pub const DEFAULT_STORAGE_ROOT: &str = "storage";
pub const DEFAULT_FILE_MAX_DATE: &str = "9999";
pub const DEFAULT_FILE_MIN_DATE: &str = "0000";


use std::io::Read;
use std::net::TcpStream;
use std::str::from_utf8;

pub fn validate_start(mut stream: &TcpStream) -> bool {
    let mut first_char = [0_u8; 1];
    let _first_n = stream.read_exact(&mut first_char).unwrap();
    let first_char = from_utf8(&first_char).unwrap_or("INVALID");
    if first_char != COM_START {
        false;
    }
    true
}

pub fn get_splits(mut stream: &TcpStream) -> String {
    let mut text_full = String::new();
    const SIZE: usize = 1024;
    let mut buf = [0_u8; SIZE];
    loop {
        let n = stream.read(&mut buf).unwrap();
        let last_char = buf[n - 1];
        if last_char == COM_END.as_bytes()[0] {
            let res = from_utf8(&buf[..n - 1]);
            match res {
                Ok(val) => text_full.push_str(val),
                Err(_val) => (),
            }

            break;
        } else {
            let text = from_utf8(&buf[..n]).unwrap();
            text_full.push_str(text);
        }
    }
    text_full
}
