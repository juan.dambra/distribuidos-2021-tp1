use crate::handlers::database::{DatabaseReadHandler, DatabaseWriteHandler};
use crate::server::Log;
use crate::managers::FileManager;
use crate::utils::{STORAGE_SERVER_PUT_PORT, STORAGE_SERVER_GET_PORT};
use ctrlc;
use std::sync::Arc;
use std::thread::spawn;

use crate::server::{Server, ServerType};

pub struct ServerMaster {
    storage_server_write: Arc<Server>,
    storage_server_read: Arc<Server>,
}

impl Log for ServerMaster {
    fn name() -> String {
        "ServerMaster".to_string()
    }
}
impl ServerMaster {
    pub fn new() -> Self {
        let storage_server_write = Arc::new(Server::new(ServerType::CUSTOM(STORAGE_SERVER_PUT_PORT)));
        let storage_server_read = Arc::new(Server::new(ServerType::CUSTOM(STORAGE_SERVER_GET_PORT)));
        return ServerMaster {
            storage_server_read,
            storage_server_write,
        };
    }

    pub fn run(&self) {
        ServerMaster::flog(format!("Starting"));
        let server_w = Arc::clone(&self.storage_server_write);
        let file_manager = FileManager::new();
        let t_server_w = spawn(move || {
            let handler = DatabaseWriteHandler { file_manager };
            server_w.run(handler);
        });
        let server_r = Arc::clone(&self.storage_server_read);
        let t_server_r = spawn(move || {
            let handler = DatabaseReadHandler {};
            server_r.run(handler);
        });

        let server_w = Arc::clone(&self.storage_server_write);
        let server_r = Arc::clone(&self.storage_server_read);
        ctrlc::set_handler(move || {
            println!("Closing Server");
            server_w.close();
            server_r.close();
        })
        .unwrap();

        t_server_w.join().unwrap();
        t_server_r.join().unwrap();

        ServerMaster::flog(format!("Closed"));
    }

}
