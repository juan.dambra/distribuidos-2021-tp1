use tp1logger::storage::ServerMaster;


fn main() {
    let server_master = ServerMaster::new();
    server_master.run();
}
