pub mod client;
pub mod handlers;
pub mod managers;
pub mod server;
pub mod storage;
pub mod storage_manager;
pub mod utils;
pub mod threadpool;

pub fn add(a: i32, b: i32) -> i32 {
    a + b
}
