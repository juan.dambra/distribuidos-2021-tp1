use crate::utils::COM_END;
use crate::utils::COM_START;
use crate::utils::DEFAULT_AMOUNT_CONNECTIONS;
use crate::utils::DEFAULT_AMOUNT_THREADS;
use crate::utils::LOG_SERVER_GET_PORT;
use crate::utils::LOG_SERVER_PUT_PORT;
use crate::utils::UNAVAILABLE_SERVER;
use std::env;
use std::io::Write;

use std::net::{SocketAddr, TcpListener, TcpStream};

use std::sync::Arc;
use std::sync::Mutex;

use crate::threadpool::ThreadPool;

pub trait Log {
    fn name() -> String {
        env::var("SERVER_NAME").unwrap_or("SERVER".to_string())
    }

    fn log(log_message: &str) {
        println!(
            "[{} - {}] {}",
            Self::name(),
            env::var("SERVER_ID").unwrap_or("1".to_string()),
            log_message
        );
    }

    fn flog(log_message: String) {
        println!(
            "[{} - {}] {}",
            Self::name(),
            env::var("SERVER_ID").unwrap_or("1".to_string()),
            log_message
        );
    }
}

pub struct Server {
    listener: Arc<TcpListener>,
    alive: Arc<Mutex<bool>>,
    addr: SocketAddr,
    amount_of_handles: usize,
}

pub enum ServerType {
    GET,
    PUT,
    WRITE,
    READ,
    CUSTOM(usize),
}

impl Log for Server {}

pub trait Handler {
    fn handle_connection(&self, stream: TcpStream);
    fn handle_error(&self, mut stream: TcpStream) {
        let msg = format!("{}{}{}", COM_START, UNAVAILABLE_SERVER, COM_END);
        stream.write_all(msg.as_bytes()).unwrap();
    }
}

impl Server {
    pub fn new(servertype: ServerType) -> Server {
        let get_port: usize = env::var("SERVER_GET_PORT")
            .unwrap_or("".to_string())
            .parse()
            .unwrap_or(LOG_SERVER_GET_PORT);
        let put_port: usize = env::var("SERVER_PUT_PORT")
            .unwrap_or("".to_string())
            .parse()
            .unwrap_or(LOG_SERVER_PUT_PORT);
        let port = match servertype {
            ServerType::GET | ServerType::READ => get_port,
            ServerType::PUT | ServerType::WRITE => put_port,
            ServerType::CUSTOM(val) => val,
        };
        let connection_string = format!("{}:{}", "0.0.0.0", port);
        Server::flog(format!("Starting server on: {}", connection_string));

        let listener = TcpListener::bind(connection_string).unwrap();

        let addr = listener.local_addr().unwrap();
        Server {
            listener: Arc::new(listener),
            alive: Arc::new(Mutex::new(true)),
            addr,
            amount_of_handles: env::var("AMOUNT_CONNECTIONS")
                .unwrap_or("".to_string())
                .parse()
                .unwrap_or(DEFAULT_AMOUNT_CONNECTIONS),
        }
    }

    pub fn close(&self) {
        let mut alive = self.alive.lock().unwrap();
        *alive = false;
        let _ = TcpStream::connect(self.addr).unwrap();
    }

    pub fn run<T: Handler + Clone + Send + 'static>(&self, handler: T) {
        Server::flog("Running Server".to_string());
        let counter = Arc::new(Mutex::new(0));
        let listener = Arc::clone(&self.listener);
        let alive = Arc::clone(&self.alive);

        let amount_of_threads = env::var("AMOUNT_THREADS")
            .unwrap_or("".to_string())
            .parse()
            .unwrap_or(DEFAULT_AMOUNT_THREADS);
        let pool = ThreadPool::new(amount_of_threads);

        for stream in listener.incoming() {
            if !*alive.lock().unwrap() {
                Server::flog("Break".to_string());
                break;
            }
            match stream {
                Ok(stream) => {
                    let peer_addr = stream.peer_addr().unwrap();
                    let counter_clone = Arc::clone(&counter);
                    let handler_clone = handler.clone();
                    let handles = self.amount_of_handles.clone();
                    pool.execute(move || {
                        let cond: bool;
                        {
                            let counter_val = *counter_clone.lock().unwrap();
                            cond = counter_val < handles;
                        }
                        match cond {
                            true => {
                                {
                                    *counter_clone.lock().unwrap() += 1;
                                    Server::flog(format!(
                                        "Adding one, current count: {}",
                                        *counter_clone.lock().unwrap()
                                    ));
                                }
                                Server::flog(format!("New connection: {}", peer_addr));
                                handler_clone.handle_connection(stream);
                                Server::flog(format!("Connection end: {}", peer_addr));
                                {
                                    *counter_clone.lock().unwrap() -= 1;
                                    Server::flog(format!(
                                        "decreasing one, current count: {}",
                                        *counter_clone.lock().unwrap()
                                    ));
                                }
                            }
                            false => {
                                handler_clone.handle_error(stream);
                                Server::flog("Too Much Connections".to_string());
                            }
                        };
                        Server::flog(format!("Count: {}", *counter_clone.lock().unwrap()));
                    });
                }
                Err(e) => {
                    Server::flog(format!("Error: {:?}", e));
                }
            }
        }
    }
}
