use crate::client::Client;
use crate::handlers::logger::ReadJob;
use crate::handlers::logger::WriteJob;
use crate::server::{Log, ServerType};
use crate::utils::STORAGE_SERVER_GET_PORT;
use crate::utils::STORAGE_SERVER_PUT_PORT;
use crate::utils::{COM_DELIMITER, COM_END, COM_START, INVALID_ID, OK_WRITE};

pub struct StorageManager;

impl Log for StorageManager {
    fn name() -> String {
        "Storage Manager".to_string()
    }
}

impl Default for StorageManager {
    fn default() -> Self {
        Self::new()
    }
}

impl StorageManager {
    pub fn new() -> Self {
        StorageManager {}
    }

    pub fn put(&self, job: WriteJob) -> Result<&str, &str> {
        let storage_client = Client::new((STORAGE_SERVER_GET_PORT, STORAGE_SERVER_PUT_PORT));
        let tags = job.tags.join(",");
        let fill_message = format!("[{}] {}", tags, job.message);
        let message = format!(
            "{start}{}{com}{}{com}{}{end}",
            job.app_id,
            job.date,
            fill_message,
            start = COM_START,
            com = COM_DELIMITER,
            end = COM_END
        );
        StorageManager::flog(format! {"Job {:?}",job});
        let response = storage_client.message(&message, ServerType::WRITE);
        StorageManager::flog(format! {"Response {:?}",response});

        match response.as_str() {
            OK_WRITE => Ok("Ok"),
            _ => Err("Err"),
        }
    }

    pub fn get(&self, job: ReadJob) -> Result<String, &str> {
        let storage_client = Client::new((STORAGE_SERVER_GET_PORT, STORAGE_SERVER_PUT_PORT));
        let from = match job.from {
            Some(date) => date.to_string(),
            None => "*".to_string(),
        };
        let to = match job.to {
            Some(date) => date.to_string(),
            None => "*".to_string(),
        };
        StorageManager::flog(format! {"Job {:?}",job});
        let message = format!(
            "{start}{}{com}{}{com}{}{com}{}{com}{}{end}",
            job.app_id,
            job.tag.unwrap_or_else(|| "*".to_string()),
            job.pattern.unwrap_or_else(|| "*".to_string()),
            from,
            to,
            start = COM_START,
            com = COM_DELIMITER,
            end = COM_END
        );
        let response = storage_client.message(&message, ServerType::READ);
        StorageManager::flog(format! {"Response {:?}",response});

        match response.as_str() {
            INVALID_ID => return Err(INVALID_ID),
            val => {
                let mut vec_splits = val
                    .split('\n')
                    .filter(|s| !s.is_empty())
                    .collect::<Vec<_>>();
                StorageManager::flog(format! {"split {:?}",vec_splits});
                vec_splits.sort();
                StorageManager::flog(format! {"sorted {:?}",vec_splits});
                let mut vec_joint = vec_splits.join("\n").to_string();
                StorageManager::flog(format! {"joint {:?}",vec_joint});
                vec_joint.push_str("\n");
                return Ok(vec_joint);
            }
        };
    }
}
