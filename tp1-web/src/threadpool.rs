use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

#[allow(dead_code)]
pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Job>,
}
//Short alias
type Job = Box<dyn FnOnce() + Send + 'static>;

impl ThreadPool {
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);
        // Create a Channel as the task sender
        let (sender, receiver) = mpsc::channel();
        //Create Channel receiver pointer
        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            //Each work shares a receiver
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    //When threadPool receives tasks, it sends Job tasks through Channel
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(job).unwrap();
    }
}

#[allow(dead_code)]
struct Worker {
    id: usize,
    thread: thread::JoinHandle<()>,
}

impl Worker {
    //Receive Receiver as closure parameter capture of thread when constructing Worker
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Job>>>) -> Worker {
        let thread = thread::spawn(move || {
            loop {
                //Call recv to block the current thread until the task is received and executed. After the execution, the loop loop is used to receive the Job from the Channel
                let result_job = receiver.lock().unwrap().recv();
                match result_job {
                    Ok(job) => {
                        println!("Worker {} got a job; executing.", id);

                        job();
                    }
                    Err(_err) => {
                        println!("Worker {} Is exiting", id);
                        return;
                    }
                }
            }
        });

        Worker { id, thread }
    }
}
