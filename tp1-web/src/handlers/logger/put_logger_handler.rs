use crate::utils::validate_start;
use crate::handlers::logger::WriteJob;
use crate::server::{Handler, Log};
use crate::storage_manager::StorageManager;
use crate::utils::{
    COM_DELIMITER, COM_END, COM_START, ERR_PUT, INCORRECT_FORMAT, OK_PUT,
    VEC_SEP,
};
use chrono::DateTime;
use std::str::FromStr;

use std::io::{Write};
use std::net::TcpStream;


const MAX_LEN_WRITE: usize = 3000;

#[derive(Clone, Debug)]
pub struct PutLoggerHandler;
impl Log for PutLoggerHandler {
    fn name() -> String {
        "PutLoggerHandler".to_string()
    }
}
impl PutLoggerHandler {
    fn parse_stream(stream: &TcpStream) -> Result<WriteJob, String> {
        if !validate_start(stream) {
            return Err("ERR".to_string());
        }
     
        let text_full = crate::utils::get_splits(stream);
        let splits: Vec<&str> = text_full.split(COM_DELIMITER).collect();
        if splits.len() < 5 {
            return Err(text_full.to_string());
        }

        PutLoggerHandler::flog(format!("Splits {:?}", splits));
        let vec_splits: Vec<String> = splits[4].split(VEC_SEP).map(|el| el.to_string()).collect();
        if !vec_splits.iter().all(|tag| {
            PutLoggerHandler::flog(format!("Splits {:?}", tag));
            tag.chars()
                .all(|v| matches!(v, '0'..='9' | 'A'..='Z' | 'a'..='z'))
        }) {
            return Err("Invalid format".to_string());
        }
        if splits[2].len() > MAX_LEN_WRITE {
            return Err("Invalid format".to_string());
        }
        let job = WriteJob {
            app_id: splits[0].to_string(),
            date: DateTime::from_str(splits[1]).unwrap(),
            message: splits[2].to_string(),
            tags: vec_splits,
        };
        PutLoggerHandler::flog(format!("Job {:?}", job));

        Ok(job)
    }
}

impl Handler for PutLoggerHandler {
    fn handle_connection(&self, mut stream: TcpStream) {
        PutLoggerHandler::log("Handling Connection");
        let job = PutLoggerHandler::parse_stream(&stream);
        let storage_manager = StorageManager {};
        match job {
            Ok(job) => {
                PutLoggerHandler::flog(format!("Final Buffer {:?}", job));
                let response = storage_manager.put(job);
                let msg = match response {
                    Ok(_val) => format!("{}{}{}", COM_START, OK_PUT, COM_END),
                    _ => format!("{}{}{}", COM_START, ERR_PUT, COM_END),
                };
                stream.write_all(msg.as_bytes()).unwrap();
            }
            Err(_val) => {
                let msg = format!("{}{}{}", COM_START, INCORRECT_FORMAT, COM_END);
                stream.write_all(msg.as_bytes()).unwrap();
            }
        }
    }
}
