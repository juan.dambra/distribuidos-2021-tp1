use chrono::DateTime;
use chrono::Utc;

mod get_logger_handler;
mod put_logger_handler;
pub use get_logger_handler::*;
pub use put_logger_handler::*;

#[derive(Debug)]
pub struct WriteJob {
    pub app_id: String,
    pub message: String,
    pub tags: Vec<String>,
    pub date: DateTime<Utc>,
}

#[derive(Debug)]
pub struct ReadJob {
    pub app_id: String,
    pub from: Option<DateTime<Utc>>,
    pub to: Option<DateTime<Utc>>,
    pub pattern: Option<String>,
    pub tag: Option<String>,
}
