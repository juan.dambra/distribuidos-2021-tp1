use crate::utils::validate_start;
use crate::handlers::logger::ReadJob;
use crate::server::{Handler, Log};
use crate::storage_manager::StorageManager;
use crate::utils::{
    CLIENT_INVALID_ID, COM_DELIMITER, COM_END, COM_START, ERR_GET, INCORRECT_FORMAT, INVALID_ID,
};
use chrono::DateTime;

use std::net::TcpStream;
use std::io::{Write};
use std::str::FromStr;


#[derive(Clone, Debug)]
pub struct GetLoggerHandler;
impl Log for GetLoggerHandler {
    fn name() -> String {
        "GetLoggerHandler".to_string()
    }
}
impl GetLoggerHandler {
    fn parse_stream(stream: &TcpStream) -> Result<ReadJob, String> {
        if !validate_start(stream) {
            return Err("ERR".to_string());
        }

        let text_full = crate::utils::get_splits(stream);
        let splits: Vec<&str> = text_full.split(COM_DELIMITER).collect();
        if splits.len() < 5 {
            return Err(text_full.to_string());
        }
        GetLoggerHandler::flog(format!("Splits {:?}", splits));

        let job = ReadJob {
            app_id: splits[0].to_string(),
            pattern: match splits[1] {
                "*" => None,
                val => Some(val.to_string()),
            },
            tag: match splits[1] {
                "*" => None,
                val => Some(val.to_string()),
            },
            from: match splits[3] {
                "*" => None,
                date => Some(DateTime::from_str(date).unwrap()),
            },
            to: match splits[4] {
                "*" => None,
                date => Some(DateTime::from_str(date).unwrap()),
            },
        };
        GetLoggerHandler::flog(format!("Job {:?}", job));

        Ok(job)
    }
}

impl Handler for GetLoggerHandler {
    fn handle_connection(&self, mut stream: TcpStream) {
        GetLoggerHandler::log("Handling Connection");
        let job = GetLoggerHandler::parse_stream(&stream);
        let storage_manager = StorageManager {};
        match job {
            Ok(job) => {
                GetLoggerHandler::flog(format!("Final Buffer {:?}", job));
                let response = storage_manager.get(job);
                GetLoggerHandler::flog(format!("Response storage manager {:?}", response));

                let msg = match response {
                    Ok(val) => format!("{}{}{}", COM_START, val, COM_END),
                    Err(INVALID_ID) => format!("{}{}{}", COM_START, CLIENT_INVALID_ID, COM_END),
                    Err(_) => format!("{}{}{}", COM_START, ERR_GET, COM_END),
                };
                stream.write_all(msg.as_bytes()).unwrap();
            }
            Err(_val) => {
                let msg = format!("{}{}{}", COM_START, INCORRECT_FORMAT, COM_END);
                stream.write_all(msg.as_bytes()).unwrap();
            }
        }
    }
}
