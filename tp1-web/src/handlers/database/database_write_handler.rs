use crate::utils::validate_start;
use crate::managers::FileManager;
use crate::server::{Handler, Log};
use crate::utils::ERR_WRITE;
use crate::utils::OK_WRITE;
use crate::utils::{COM_DELIMITER, COM_END, COM_START, INCORRECT_FORMAT};
use chrono::DateTime;
use chrono::Utc;
use std::io::{Write};
use std::net::TcpStream;
use std::path::PathBuf;

use std::str::FromStr;

#[derive(Debug)]
pub struct DatabaseWriteJob {
    pub app_id: String,
    pub message: String,
    pub date: DateTime<Utc>,
}


#[derive(Clone, Debug)]
pub struct DatabaseWriteHandler {
    pub file_manager: FileManager,
}

impl Log for DatabaseWriteHandler {
    fn name() -> String {
        "DatabaseWriteHandler".to_string()
    }
}
impl DatabaseWriteHandler {
    fn parse_stream(stream: &TcpStream) -> Result<DatabaseWriteJob, String> {
        if !validate_start(stream) {
            return Err("ERR".to_string());
        }
        
        let text_full = crate::utils::get_splits(stream);
        let splits: Vec<&str> = text_full.split(COM_DELIMITER).collect();
        DatabaseWriteHandler::flog(format!("Splits {:?}", splits));

        if splits.len() < 3 {
            DatabaseWriteHandler::log("Error in Format");
            return Err(text_full.to_string());
        }
        let job = DatabaseWriteJob {
            app_id: splits[0].to_string(),
            date: DateTime::from_str(splits[1]).unwrap(),
            message: splits[2].to_string(),
        };
        DatabaseWriteHandler::flog(format!("Job {:?}", job));

        Ok(job)
    }
}

impl Handler for DatabaseWriteHandler {
    fn handle_connection(&self, mut stream: TcpStream) {
        DatabaseWriteHandler::log("Handling Connection");
        let job = DatabaseWriteHandler::parse_stream(&stream);

        match job {
            Ok(mut job) => {
                DatabaseWriteHandler::flog(format!("Write Job {:?}", job));
                let app_id = job.app_id.as_str();
                let date = job.date.date().to_string();
                let date = date.strip_suffix("UTC").unwrap();
                let mut entries: Vec<PathBuf> = FileManager::filter_by_id_date(app_id, date);
                DatabaseWriteHandler::flog(format!("Entries Unsorted {:?}", entries));
                if job.message.chars().last().unwrap() != '\n' {
                    job.message.push_str("\n");
                }
                let mut entry = String::new();
                if entries.len() > 0 {
                    entries.sort();
                    let entry_path = &entries[0].to_str().unwrap();
                    entry.push_str(entry_path);
                } else {
                    let entry_path = FileManager::create_by_id_date(app_id, date);
                    match entry_path {
                        Ok(entrypath) => {
                            let entry_path = entrypath.to_str().unwrap();
                            entry.push_str(entry_path);
                        }
                        Err(_) => {
                            let msg = format!("{}{}{}", COM_START, ERR_WRITE, COM_END);
                            stream.write_all(msg.as_bytes()).unwrap();
                            return;
                        }
                    }
                }
                let message = format!("[{}] {} {}", job.app_id, job.date, job.message);
                let res = self.file_manager.append(&entry, message);
                match res {
                    Ok(_val) => {
                        let msg = format!("{}{}{}", COM_START, OK_WRITE, COM_END);
                        stream.write_all(msg.as_bytes()).unwrap();
                    }
                    Err(_) => {
                        let msg = format!("{}{}{}", COM_START, ERR_WRITE, COM_END);
                        stream.write_all(msg.as_bytes()).unwrap();
                        return;
                    }
                }
            }
            Err(_val) => {
                let msg = format!("{}{}{}", COM_START, INCORRECT_FORMAT, COM_END);
                stream.write_all(msg.as_bytes()).unwrap();
            }
        }
    }

}
