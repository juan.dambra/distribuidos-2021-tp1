use crate::managers::FileManager;
use crate::server::{Handler, Log};
use crate::utils::INVALID_ID;
use std::io::{BufRead, BufReader};

use crate::utils::{validate_start, COM_DELIMITER, COM_END, COM_START, INCORRECT_FORMAT};
use chrono::DateTime;
use chrono::Utc;
use std::io::Write;
use std::net::TcpStream;

use std::str::FromStr;

#[derive(Debug)]
pub enum MessagePattern {
    All,
    Word(String),
}

#[derive(Debug)]
pub struct DatabaseReadJob {
    pub app_id: String,
    pub message_pattern: MessagePattern,
    pub tag: Option<String>,
    pub from: Option<DateTime<Utc>>,
    pub to: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug)]
pub struct DatabaseReadHandler;
impl Log for DatabaseReadHandler {
    fn name() -> String {
        "DatabaseReadHandler".to_string()
    }
}
impl DatabaseReadHandler {
    fn parse_stream(stream: &TcpStream) -> Result<DatabaseReadJob, String> {
        if !validate_start(stream) {
            return Err("ERR".to_string());
        }
        let text_full = crate::utils::get_splits(stream);
        let splits: Vec<&str> = text_full.split(COM_DELIMITER).collect();
        DatabaseReadHandler::flog(format!("Splits {:?}", splits));

        if splits.len() < 5 {
            DatabaseReadHandler::log("Error in Format");
            return Err(text_full.to_string());
        }

        let job = DatabaseReadJob {
            app_id: splits[0].to_string(),
            message_pattern: match splits[2] {
                "*" => MessagePattern::All,
                val => MessagePattern::Word(val.to_string()),
            },
            tag: match splits[1] {
                "*" => None,
                val => Some(val.to_string()),
            },
            from: match splits[3] {
                "*" => None,
                date => Some(DateTime::from_str(date).unwrap()),
            },
            to: match splits[4] {
                "*" => None,
                date => Some(DateTime::from_str(date).unwrap()),
            },
        };
        DatabaseReadHandler::flog(format!("Job {:?}", job));

        Ok(job)
    }
}

fn filter_line_by_tag(line: &str, tag: &str) -> bool {
    if line.len() > 0 {
        let start = line.find("UTC").unwrap();
        let end = line[start..].find("]").unwrap();
        let substring = &line[start + 5..start + end];
        let tags: Vec<&str> = substring.split(",").collect();
        if tags.iter().any(|&i| i == tag) {
            DatabaseReadHandler::flog(format!("Found line {:?}", line));
            return true;
        }
    }
    return false;
}

fn filter_line_by_word(line: &str, word: &str) -> bool {
    if line.len() > 0 {
        let start = line.find("UTC").unwrap();
        let end = line[start..].find("]").unwrap();
        let substring = &line[start + end..];
        DatabaseReadHandler::flog(format!("Substring {:?}", substring));

        return substring.contains(&word);
    }
    return false;
}

impl Handler for DatabaseReadHandler {
    fn handle_connection(&self, mut stream: TcpStream) {
        DatabaseReadHandler::log("Handling Connection");
        let job = DatabaseReadHandler::parse_stream(&stream);
        match job {
            Ok(job) => {
                let id_exists = FileManager::id_exists(&job.app_id);
                if !id_exists {
                    let msg = format!("{}{}{}", COM_START, INVALID_ID, COM_END);
                    stream.write_all(msg.as_bytes()).unwrap();
                    return;
                }

                let files = FileManager::filter_by_id_daterange(&job.app_id, (job.from, job.to));
                DatabaseReadHandler::flog(format!("Files found {:?}", files));
                stream.write(COM_START.as_bytes()).unwrap();
                let tag = job.tag;
                let message_pattern = job.message_pattern;
                for file in files {
                    let file_open = FileManager::open_read_file(file.to_str().unwrap());
                    let buf = BufReader::new(file_open);

                    match (&tag, &message_pattern) {
                        (None, MessagePattern::All) => {
                            for line in buf.lines() {
                                stream.write_all(line.unwrap().as_bytes()).unwrap();
                                stream.write("\n".as_bytes()).unwrap();
                            }
                        }
                        (Some(ref tag), MessagePattern::All) => {
                            let line_map = buf.lines().map(|line| line.unwrap());

                            let line = line_map.filter(|line| return filter_line_by_tag(line, tag));
                            for line_i in line {
                                let new_line = format!("{}\n", line_i);
                                stream.write_all(new_line.as_bytes()).unwrap();
                            }
                        }
                        (None, MessagePattern::Word(word)) => {
                            let line_map = buf.lines().map(|line| line.unwrap());

                            let line =
                                line_map.filter(|line| return filter_line_by_word(line, word));
                            for line_i in line {
                                let new_line = format!("{}\n", line_i);
                                stream.write_all(new_line.as_bytes()).unwrap();
                            }
                        }
                        (Some(ref tag), MessagePattern::Word(word)) => {
                            let line_map = buf.lines().map(|line| line.unwrap());

                            let line = line_map
                                .filter(|line| return filter_line_by_tag(line, tag))
                                .filter(|line| return filter_line_by_word(line, word));

                            for line_i in line {
                                let new_line = format!("{}\n", line_i);
                                stream.write_all(new_line.as_bytes()).unwrap();
                            }
                        }
                    }
                }

                DatabaseReadHandler::log("Files Written");
                stream.write(COM_END.as_bytes()).unwrap();
            }
            Err(_val) => {
                let msg = format!("{}{}{}", COM_START, INCORRECT_FORMAT, COM_END);
                stream.write_all(msg.as_bytes()).unwrap();
            }
        }
    }
}
