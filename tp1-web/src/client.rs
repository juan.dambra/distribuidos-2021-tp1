use crate::utils::validate_start;
use super::utils::{COM_DELIMITER, COM_END, COM_START, ERR_PUT, VEC_SEP};
use crate::server::Log;
use crate::server::ServerType;
use crate::utils::UNAVAILABLE_SERVER;
use chrono::DateTime;
use chrono::Utc;
use std::env;
use std::io::{Read, Write};
use std::net::TcpStream;
use std::str::from_utf8;

pub struct Client {
    get_port: usize,
    put_port: usize,
}

impl Log for Client {
    fn name() -> String {
        "CLIENT".to_string()
    }
}
impl Client {
    pub fn new(ports: (usize, usize)) -> Client {
        Client {
            get_port: ports.0,
            put_port: ports.1,
        }
    }

    pub fn ping(&self) -> String {
        let msg = format!("{}PING{}", COM_START, COM_END);
        return self.message(msg.as_str(), ServerType::PUT);
    }

    pub fn message(&self, string: &str, port: ServerType) -> String {
        let server_port = match port {
            ServerType::GET | ServerType::READ => self.get_port,
            ServerType::PUT | ServerType::WRITE => self.put_port,
            ServerType::CUSTOM(val) => val,
        };
        let host = match port {
            ServerType::GET => env::var("GET_SERVER_HOST").unwrap_or("localhost".to_string()),
            ServerType::PUT => env::var("PUT_SERVER_HOST").unwrap_or("localhost".to_string()),
            ServerType::READ | ServerType::WRITE => {
                env::var("STORAGE_SERVER_HOST").unwrap_or("localhost".to_string())
            }
            _ => "localhost".to_string(),
        };

        let connection_string = format!("{}:{}", host, server_port);
        Client::flog(format!("Connecting to {}", connection_string));
        match TcpStream::connect(connection_string) {
            Ok(mut stream) => {
                Client::flog(format!(
                    "Successfully connected to server in port {}",
                    self.put_port
                ));
                let res = stream.write_all(string.as_bytes());
                match res {
                    Ok(_) => {
                        Client::flog(format!("Sent message {}, awaiting reply...", string));
                    }
                    Err(e) => {
                        Client::flog(format!("Failed to write {}", e));
                        return UNAVAILABLE_SERVER.to_string();
                    }
                };
                let _full_string = String::new();

                if !validate_start(&stream) {
                    return "ERR".to_string();
                }
                let mut text_full = String::new();
                const SIZE: usize = 1024;
                let mut buf = [0_u8; SIZE];
                loop {
                    let n = stream.read(&mut buf).unwrap();
                    let last_char = buf[n - 1];
                    Client::flog(format!(
                        "Bytes read {:?}, lastChar {:?} END: {:?}",
                        n,
                        last_char,
                        COM_END.as_bytes()
                    ));
                    if last_char == COM_END.as_bytes()[0] {
                        Client::log("End of string");
                        let res = from_utf8(&buf[..n - 1]);
                        match res {
                            Ok(val) => text_full.push_str(val),
                            Err(val) => Client::flog(format!("End of string error {}", val)),
                        }
                        break;
                    } else {
                        let text = from_utf8(&buf[..n]).unwrap();
                        text_full.push_str(text);
                    }
                }
                return text_full;
            }
            Err(e) => {
                Client::flog(format!("Failed to connect {}", e));
                UNAVAILABLE_SERVER.to_string()
            }
        }
    }

    pub fn put_log(
        &self,
        app_id: &str,
        date: DateTime<Utc>,
        message: &str,
        tags: Vec<&str>,
    ) -> String {
        let vec_len = tags.len();
        let mut vec_encoded = tags.join(VEC_SEP);
        vec_encoded.push_str(COM_DELIMITER);

        let full_message = format!(
            "{start}{}{com}{}{com}{}{com}{}{com}{}{end}",
            app_id,
            date,
            message,
            vec_len,
            vec_encoded,
            start = COM_START,
            com = COM_DELIMITER,
            end = COM_END
        );
        let response = self.message(&full_message, ServerType::PUT);
        Client::flog(format!("RESPONSE {:?}", response));
        let res = match response.as_str() {
            ERR_PUT => UNAVAILABLE_SERVER.to_string(),
            val => val.to_string(),
        };
        return res;
    }

    pub fn get_log(
        &self,
        app_id: &str,
        pattern: Option<&str>,
        tag: Option<&str>,
        from: Option<DateTime<Utc>>,
        to: Option<DateTime<Utc>>,
    ) -> String {
        let from = match from {
            Some(date) => date.to_string(),
            None => "*".to_string(),
        };
        let to = match to {
            Some(date) => date.to_string(),
            None => "*".to_string(),
        };
        let message = format!(
            "{start}{}{com}{}{com}{}{com}{}{com}{}{end}",
            app_id,
            pattern.unwrap_or("*"),
            tag.unwrap_or("*"),
            from,
            to,
            start = COM_START,
            com = COM_DELIMITER,
            end = COM_END
        );

        let response = self.message(&message, ServerType::GET);
        return response;
    }
}
