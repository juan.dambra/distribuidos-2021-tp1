
use std::sync::Arc;
use tp1logger::handlers::logger::PutLoggerHandler;

use tp1logger::server::{Server, ServerType};

fn main() {
    let server = Arc::new(Server::new(ServerType::PUT));
    println!("Server Running");
    let server_arc = Arc::clone(&server);
    let t = std::thread::spawn(move || {
        let handler = PutLoggerHandler {};
        server_arc.run(handler);
    });

    ctrlc::set_handler(move || {
        println!("Closing Server");
        server.close()
    })
    .unwrap();
    t.join().unwrap();
}
