use crate::utils::DEFAULT_FILE_MIN_DATE;
use crate::utils::DEFAULT_FILE_MAX_DATE;
use crate::server::Log;
use crate::utils::DEFAULT_STORAGE_ROOT;
use chrono::{DateTime, Utc};
use std::collections::HashMap;
use std::fs::read_dir;
use std::fs::DirEntry;
use std::fs::File;
use std::fs::OpenOptions;
use std::sync::Arc;
use std::sync::Mutex;

use std::io::{Error, Write};
use std::path::PathBuf;

#[derive(Clone, Debug)]
pub struct FileManager {
    locks: Arc<Mutex<HashMap<String, Arc<Mutex<bool>>>>>,
}

impl Log for FileManager {
    fn name() -> String {
        return "FileManager".to_string();
    }
}

impl FileManager {
    pub fn new() -> Self {
        FileManager {
            locks: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub fn open_read_file(filename: &str) -> File {
        let f = File::open(filename).unwrap();
        return f;
    }
    pub fn id_exists(app_id: &str) -> bool {
        let storage_root = DEFAULT_STORAGE_ROOT;
        let file_query = format!("{}/{}-", storage_root, app_id);
        FileManager::flog(format!("File Query {:?}", file_query));
        let entries: Vec<PathBuf> = read_dir(storage_root)
            .unwrap()
            .filter_map(|res| {
                res.map(|e: DirEntry| {
                    return e.path();
                })
                .ok()
            })
            .filter(|path| {
                return path.to_str().unwrap().starts_with(&file_query);
            })
            .collect();
        let exists = entries.len() > 0;
        return exists;
    }

    pub fn find_by_id(app_id: String) -> Vec<PathBuf> {
        let storage_root = DEFAULT_STORAGE_ROOT;
        let file_query = format!("{}/{}-", storage_root, app_id);
        FileManager::flog(format!("File Query {:?}", file_query));
        let entries = read_dir(storage_root)
            .unwrap()
            .filter_map(|res| {
                res.map(|e: DirEntry| {
                    return e.path();
                })
                .ok()
            })
            .filter(|path| {
                return path.to_str().unwrap().starts_with(&file_query);
            })
            .collect();
        return entries;
    }

    pub fn filter_by_id_date(app_id: &str, date: &str) -> Vec<PathBuf> {
        let storage_root = DEFAULT_STORAGE_ROOT;
        let file_query = format!("{}/{}-{}.log", storage_root, app_id, date);
        FileManager::flog(format!("File Query {:?}", file_query));
        let entries = read_dir(storage_root)
            .unwrap()
            .filter_map(|res| {
                res.map(|e: DirEntry| {
                    return e.path();
                })
                .ok()
            })
            .filter(|path| {
                return path.to_str().unwrap().starts_with(&file_query);
            })
            .collect();
        return entries;
    }
    pub fn filter_by_id_daterange(
        app_id: &str,
        range: (Option<DateTime<Utc>>, Option<DateTime<Utc>>),
    ) -> Vec<PathBuf> {
        let storage_root = DEFAULT_STORAGE_ROOT;
        let file_query = format!("{}/{}-", storage_root, app_id);

        let mut from_date = String::new();
        match range.0 {
            Some(date) => {
                from_date = date
                    .date()
                    .to_string()
                    .strip_suffix("UTC")
                    .unwrap()
                    .to_string();
            }
            None => from_date.push_str(DEFAULT_FILE_MIN_DATE),
        };

        let mut to_date = String::new();
        match range.1 {
            Some(date) => {
                to_date = date
                    .date()
                    .to_string()
                    .strip_suffix("UTC")
                    .unwrap()
                    .to_string();
            }
            None => to_date.push_str(DEFAULT_FILE_MAX_DATE),
        };

        let file_query_from = format!("{}/{}-{}.log", storage_root, app_id, from_date);
        let file_query_to = format!("{}/{}-{}.log", storage_root, app_id, to_date);

        FileManager::flog(format!("File Query {:?}", file_query));
        let entries = read_dir(storage_root)
            .unwrap()
            .filter_map(|res| {
                res.map(|e: DirEntry| {
                    return e.path();
                })
                .ok()
            })
            .filter(|path| {
                return path.to_str().unwrap().starts_with(&file_query);
            })
            .filter(|path| {
                return path.to_str().unwrap() > &file_query_from
                    && path.to_str().unwrap() <= &file_query_to;
            })
            .collect();
        return entries;
    }

    pub fn create_by_id_date(app_id: &str, date: &str) -> Result<PathBuf, String> {
        let storage_root = DEFAULT_STORAGE_ROOT;
        let file_query = format!("{}/{}-{}.log", storage_root, app_id, date);
        let path = PathBuf::from(file_query.clone());
        let file = File::create(file_query);
        match file {
            Ok(_file) => return Ok(path),
            Err(_err) => return Err("Error creating file".to_string()),
        }
    }

    fn get_lock(&self, filename: &str) -> Result<Arc<Mutex<bool>>, String> {
        let mut map = self.locks.lock().unwrap();
        if !map.contains_key(filename) {
            map.insert(filename.to_string(), Arc::new(Mutex::new(true)));
            FileManager::flog(format!("No Key!"));
        }
        match map.get(filename) {
            Some(file) => Ok(Arc::clone(file)),
            None => Err("Error".to_string()),
        }
    }

    pub fn append(&self, filename: &str, message: String) -> Result<(), Error> {
        let file_lock = self.get_lock(filename).unwrap();
        let _res = file_lock.lock();
        let mut file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();
        let res = file.write_all(message.as_bytes());
        return res;
    }
}
